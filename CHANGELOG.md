# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this project adheres to
[Semantic Versioning](https://semver.org).

## [Unreleased]

### Added
- Language component.
- Language model context.
- Language model accessor.
- Patient unique component.
- Patient multiple component.
- Wheelchair vector component.
- [Lodash](https://www.npmjs.com/package/lodash) as dependency.
- Menu component links:
  - ```/dashboard```
  - ```/patients```
  - ```/settings/profile```
  - ```/settings/display```
  - ```/settings/security```
  - ```/settings/notifications```
  - ```/help```
- Record model definition.
- Person model definition.
- Patient model definition.
- Connection factory.
- Connection response interceptor for date parser.
- Connection response interceptor for create notification.
- Connection response interceptor for delete notification.
- Connection response interceptor for update notification.

### Changed
- Docker image based on Influenza 1.0.3.
- Update of all dependencies managed by npm.
- Specification of Node 14.5.0 as a Continuous Integration image.
- Specification of Docker 19.03.12 as a Continuous Integration image.
- Specification of Docker DinD 19.03.12 as a Continuous Integration service.
- `API_BASE_URL` environment variable renamed to `API_PATH`.
- `ROUTER_BASE_PATH` environment variable renamed to `ROUTER_PATH`.
- `ROUTER_HISTORY_TYPE` environment variable renamed to `ROUTER_HISTORY`.

### Removed
- Configuration component.
- Tests for Menu component.
- Header from Menu component.
- Padding from Drawer component body.

## [0.3.2] - 2020-06-03

### Changed
- Docker image based on Influenza 1.0.2.
- Specification of Node 14.4.0 as a Continuous Integration image.
- Specification of Docker 19.03.11 as a Continuous Integration image.
- Specification of Docker DinD 19.03.11 as a Continuous Integration service.
- Update of all dependencies managed by npm.

## [0.3.1] - 2020-05-28

### Added
- Visual feedback for email address validation process in recovery component.

### Changed
- Form reference as read only in recovery component.
- Update of all dependencies managed by npm.

### Fixed
- Unnecessary creation of resources when validating email address in recovery component.

## [0.3.0] - 2020-05-27

### Added
- Recovery component.
- Visual feedback for form validation status.
- Automatic scrolling when validating form fields.

### Changed
- Docker image based on Influenza 1.0.1.
- Specification of Node 14.3.0 as a Continuous Integration image.
- Specification of Docker 19.03.9 as a Continuous Integration image.
- Specification of Docker DinD 19.03.9 as a Continuous Integration service.
- Update of all dependencies managed by npm.
- Allow use of curly brackets in strings.
- Allow declaration of unused parameters.
- Mandatory fields for all model definitions.
- Authentication message incorporated to fields in login component.
- Overwrite default validation messages.
- Improve conflict message in register component.

## [0.2.1] - 2020-05-25

### Added
- Transpilation with support for class properties.
- Camel Case exception for optional member expressions.
- Credential model definition.

### Changed
- Mitigate interactivity of error message in login component.
- Mitigate interactivity of success message in register component.
- Conflict message incorporated to email field in register component.
- Optional fields in session model definition.
- Match all routes exactly.
- Update of all dependencies managed by npm.
- Compress files on development server.
- Make development server available on local network.

### Removed
- Property for success event handle from register component.

## [0.2.0] - 2020-05-19

### Added
- Register component.

### Changed
- Update of all dependencies managed by npm.

## [0.1.0] - 2020-05-18

### Added
- Redirect component.
- Dashboard component.
- Login component.
- Connection component.
- Connection model context.
- Connection model accessor.
- `API_BASE_URL` environment variable to change API base URL.
- [axios](https://www.npmjs.com/package/axios) as dependency.
- Session model definition.
- `THEME_COLOR_PROCESSING` environment variable to change processing color of theme.
- `THEME_COLOR_INFORMATION` environment variable to change information color of theme.
- `THEME_COLOR_HIGHLIGHT` environment variable to change highlight color of theme.
- Configuration component.
- Account model definition.
- Menu component.
- Type definition for environment variables.
- Router component.
- Docker image based on Influenza 1.0.0.
- Served build stage to Continuous Integration.
- `ROUTER_HISTORY_TYPE` environment variable to change router history type.
- `ROUTER_BASE_PATH` environment variable to change router base path.
- React Router configuration.
- Contained build stage to Continuous Integration.
- Documentation stage to Continuous Integration.
- Specification of Node 14.1.0 as a Continuous Integration image.
- Specification of Docker 19.03.8 as a Continuous Integration image.
- Specification of Docker DinD 19.03.8 as a Continuous Integration service.
- Test stage to Continuous Integration.
- Continuous Integration.
- [stylelint Order](https://www.npmjs.com/package/stylelint-order) as development dependency.
- Content component.
- Header component.
- Default entrypoint.
- Layout component.
- [Identity Object Proxy](https://www.npmjs.com/package/identity-obj-proxy) as development dependency.
- [React Test Renderer](https://www.npmjs.com/package/react-test-renderer) as development dependency.
- [React Router](https://www.npmjs.com/package/react-router-dom) as dependency.
- [Babel Plugin Export Namespace From](https://www.npmjs.com/package/@babel/plugin-proposal-export-namespace-from) as
  development dependency.
- [Ant Design Icons for React](https://www.npmjs.com/package/@ant-design/icons) as dependency.
- Documentation script.
- Bundle documentation script.
- Analyze bundle documentation script.
- Profile bundle documentation script.
- [webpack Bundle Analyzer](https://www.npmjs.com/package/webpack-bundle-analyzer) as development dependency.
- Jest specification test script.
- Jest configuration.
- [Jest](https://www.npmjs.com/package/jest) as development dependency.
- Test script.
- Specification test script.
- TypeScript specification test script.
- Module documentation script.
- TypeDoc configuration.
- [TypeDoc](https://www.npmjs.com/package/typedoc) as development dependency.
- Format test script.
- TypeScript format test script.
- Less format test script.
- stylelint configuration.
- [stylelint](https://www.npmjs.com/package/stylelint) as development dependency.
- Start script.
- Build script.
- [webpack Development Server](https://www.npmjs.com/package/webpack-dev-server) as development dependency.
- `APP_NAME` environment variable to change app name.
- `THEME_COLOR_ERROR` environment variable to change error color of theme.
- `THEME_COLOR_WARNING` environment variable to change warning color of theme.
- `THEME_COLOR_SUCCESS` environment variable to change success color of theme.
- `THEME_COLOR_PRIMARY` environment variable to change primary color of theme.
- Babel Plugin Import configuration.
- [Babel Plugin Import](https://www.npmjs.com/package/babel-plugin-import) as development dependency.
- [Ant Design](https://www.npmjs.com/package/antd) as dependency.
- HTML Webpack Plugin configuration.
- [HTML Webpack Plugin](https://www.npmjs.com/package/html-webpack-plugin) as development dependency.
- Optimize CSS Assets Webpack Plugin configuration.
- UglifyJS Webpack Plugin configuration.
- [Optimize CSS Assets Webpack Plugin](https://www.npmjs.com/package/optimize-css-assets-webpack-plugin) as development
  dependency.
- [UglifyJS Webpack Plugin](https://www.npmjs.com/package/uglifyjs-webpack-plugin) as development dependency.
- Mini CSS Extract Plugin configuration.
- CSS Loader configuration.
- Less Loader configuration.
- [Mini CSS Extract Plugin](https://www.npmjs.com/package/mini-css-extract-plugin) as development dependency.
- [CSS Loader](https://www.npmjs.com/package/css-loader) as development dependency.
- [Less Loader](https://www.npmjs.com/package/less-loader) as development dependency.
- [Less](https://www.npmjs.com/package/less) as development dependency.
- Babel Loader configuration.
- [Babel Loader](https://www.npmjs.com/package/babel-loader) as development dependency.
- webpack configuration.
- ESLint configuration.
- TypeScript configuration.
- [ESLint](https://www.npmjs.com/package/eslint) as development dependency.
- [React DOM](https://www.npmjs.com/package/react-dom) as dependency.
- [React](https://www.npmjs.com/package/react) as dependency.
- [TypeScript](https://www.npmjs.com/package/typescript) as development dependency.
- [webpack CLI](https://www.npmjs.com/package/webpack-cli) as development dependency.
- [webpack](https://www.npmjs.com/package/webpack) as development dependency.
- Manager for the project and its dependencies.
- Project license.
- Contributing guide.
- Record of all notable changes made to this project.
