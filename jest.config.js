module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    'source/**/*.{ts,tsx}'
  ],
  coverageDirectory: 'coverage',
  coverageReporters: [
    'html',
    'text'
  ],
  moduleNameMapper: {
    '\\.less$': 'identity-obj-proxy'
  },
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  verbose: true
}
