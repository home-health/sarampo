declare namespace NodeJS {
  export interface ProcessEnv {
    API_PATH: string
    APP_NAME: string
    ROUTER_PATH: string
    ROUTER_HISTORY: 'browser' | 'hash' | 'memory'
    THEME_COLOR_ERROR: string
    THEME_COLOR_HIGHLIGHT: string
    THEME_COLOR_INFORMATION: string
    THEME_COLOR_PRIMARY: string
    THEME_COLOR_PROCESSING: string
    THEME_COLOR_SUCCESS: string
    THEME_COLOR_WARNING: string
  }
}
