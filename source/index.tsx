import React from 'react'
import { Router } from './component'
import { render } from 'react-dom'

window.addEventListener('DOMContentLoaded', () => {
  render(
    <Router.Definition
      history={process.env.ROUTER_HISTORY}
    />,
    document.body
  )
})
