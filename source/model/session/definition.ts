import { Record } from '../'

export interface Definition extends Record.Definition {
  credential: string
}
