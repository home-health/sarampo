import { Converter } from './covenant'
import Moment from 'moment'

export const Date: Converter = value => typeof value === 'string' && Moment(value).isValid() ? Moment(value) : value
