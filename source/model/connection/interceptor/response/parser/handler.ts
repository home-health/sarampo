import { AxiosResponse } from 'axios'
import { Date } from './converter'
import { mapValues } from 'lodash'

const converters = [
  Date
]

export const Handler = (response: AxiosResponse): AxiosResponse => {
  const { data } = response
  if (data) {
    converters.forEach(converter => {
      response.data = Array.isArray(data) ? data.map(item => mapValues(item, converter)) : mapValues(data, converter)
    })
  }
  return response
}
