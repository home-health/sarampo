import { Filter } from './covenant'
import { message } from 'antd'

export const Update: Filter = {
  execute: () => message.success('Atualização concluída'),
  ignored: [
    /^\/sessions$/g
  ],
  methods: [
    'patch',
    'put'
  ]
}
