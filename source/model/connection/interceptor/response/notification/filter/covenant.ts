import { Method } from 'axios'

export interface Filter {
  execute: () => void
  ignored: Array<RegExp>
  methods: Array<Method>
}
