import { Filter } from './covenant'
import { message } from 'antd'

export const Create: Filter = {
  execute: () => message.success('Criação concluída'),
  ignored: [
    /^\/sessions$/g,
    /^\/validations$/g
  ],
  methods: [
    'post'
  ]
}
