import { Filter } from './covenant'
import { message } from 'antd'

export const Delete: Filter = {
  execute: () => message.success('Remoção concluída'),
  ignored: [],
  methods: [
    'delete'
  ]
}
