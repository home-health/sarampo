import { Create, Delete, Update } from './filter'
import { AxiosResponse } from 'axios'

const filters = [
  Create,
  Delete,
  Update
]

export const Handler = (response: AxiosResponse): AxiosResponse => {
  const { config: { method, url }, status } = response
  filters.filter(filter => {
    return method && url && status > 200 && status < 300 && filter.methods.includes(method) && filter.ignored.filter(pattern => pattern.test(url)).length === 0
  }).forEach(filter => {
    filter.execute()
  })
  return response
}
