import { AxiosInstance } from 'axios'

export interface Accessor {
  authorized: () => boolean
  connection: AxiosInstance
  update: (connection: AxiosInstance) => void
}
