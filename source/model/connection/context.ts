import { Accessor } from './accessor'
import { Factory } from './factory'
import { createContext } from 'react'

export const Context = createContext<Accessor>({
  authorized: () => true,
  connection: new Factory().create(),
  update: () => {
    throw new Error('Not implemented.')
  }
})
