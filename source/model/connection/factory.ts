import Axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
import { Interceptor } from './'

export class Factory {
  private preset: AxiosRequestConfig

  constructor (preset?: AxiosRequestConfig) {
    this.preset = preset ?? Axios.defaults
  }

  public create (): AxiosInstance {
    return this.configure(Axios.create(this.getPreset()))
  }

  public configure (instance: AxiosInstance): AxiosInstance {
    this.getResponseInterceptors().forEach(interceptor => instance.interceptors.response.use(interceptor))
    return instance
  }

  public getResponseInterceptors (): Array<((response: AxiosResponse) => AxiosResponse)> {
    return [
      Interceptor.Response.Notification.Handler,
      Interceptor.Response.Parser.Handler
    ]
  }

  public getPreset (): AxiosRequestConfig {
    return {
      ...this.preset,
      baseURL: process.env.API_PATH
    }
  }

  public setPreset (preset: AxiosRequestConfig): this {
    this.preset = preset
    return this
  }
}
