export interface Definition {
  account_identifier: number
  exp: number
  iat: number
  jti: number
}
