import { Moment } from 'moment'
import { Record } from '../'

export interface Definition extends Record.Definition {
  birth: Moment
  ethnicity: 'black' | 'brown' | 'indigenous' | 'white' | 'yellow'
  first_name: string
  gender: 'female' | 'male' | 'other'
  last_name: string
  marital_status: 'divorced' | 'married' | 'separated' | 'single' | 'widowed'
}
