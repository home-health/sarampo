import { Accessor } from './accessor'
import { Resource } from './'
import { createContext } from 'react'

export const Context = createContext<Accessor>({
  locale: Resource.Brazil.Portuguese,
  update: () => {
    throw new Error('Not implemented.')
  }
})
