import { Locale } from 'antd/lib/locale-provider'
import Resource from 'antd/lib/locale/pt_BR'
import { merge } from 'lodash'

export const Portuguese: Locale = merge(
  Resource,
  {
    DatePicker: {
      lang: {
        placeholder: ''
      }
    },
    Form: {
      defaultValidateMessages: {
        required: 'Preencha este campo',
        string: {
          min: 'Informe no mínimo ${min} caracteres'
        },
        types: {
          email: 'Formato inválido'
        }
      }
    }
  }
)
