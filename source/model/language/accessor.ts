import { Locale } from 'antd/lib/locale-provider'

export interface Accessor {
  locale: Locale
  update: (locale: Locale) => void
}
