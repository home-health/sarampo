import { Moment } from 'moment'

export interface Definition {
  created_at: Moment
  identifier: number
  updated_at: Moment
}
