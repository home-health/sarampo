import { Record } from '../'

export interface Definition extends Record.Definition {
  email_address: string
  email_verified: boolean
  password: string
}
