import './appearance.less'
import { Button, DatePicker, Empty, Form, Input, Modal, PageHeader, Select } from 'antd'
import { ConfigConsumer, ConfigConsumerProps } from 'antd/lib/config-provider'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Properties, State, Type } from './'
import React, { Component, ReactNode, createRef } from 'react'
import { AxiosResponse } from 'axios'
import { FormInstance } from 'antd/lib/form'
import { Redirect } from 'react-router-dom'
import { boundMethod } from 'autobind-decorator'
import { omitBy } from 'lodash'

export class Definition extends Component<Properties, State> {
  private readonly form = createRef<FormInstance>()

  constructor (properties: Properties) {
    super(properties)
    this.state = {
      blocked: this.props.match.params.patient_identifier !== undefined,
      loading: this.props.match.params.patient_identifier !== undefined,
      patient: undefined,
      regress: false
    }
  }

  public componentDidMount (): void {
    if (this.props.match.params.patient_identifier) {
      this.fetch({
        identifier: parseInt(this.props.match.params.patient_identifier)
      }).then(this.handleSuccess).catch(this.handleError)
    }
  }

  @boundMethod
  public create (patient: Partial<Type.Patient>): Promise<AxiosResponse> {
    return this.props.connection.post('/patients', patient)
  }

  @boundMethod
  public delete (patient: Pick<Type.Patient, 'identifier'>): Promise<AxiosResponse> {
    return this.props.connection.delete(`/patients/${patient.identifier}`)
  }

  @boundMethod
  public difference (patient: Partial<Type.Patient>): Promise<Partial<Type.Patient>> {
    return new Promise((resolve: (value: Partial<Type.Patient>) => void) => {
      resolve(omitBy(patient, (value, key) => this.state.patient && this.state.patient[key] === value))
    })
  }

  @boundMethod
  public fetch (patient: Pick<Type.Patient, 'identifier'>): Promise<AxiosResponse<Type.Patient>> {
    return this.props.connection.get(`/patients/${patient.identifier}`, {
      params: {
        fields: [
          'birth',
          'ethnicity',
          'first_name',
          'gender',
          'identifier',
          'last_name',
          'marital_status'
        ]
      }
    })
  }

  @boundMethod
  public handleBack (): void {
    this.setState({
      regress: true
    })
  }

  @boundMethod
  public handleDelete (): void {
    Modal.confirm({
      okType: 'danger',
      onOk: () => this.state.patient && this.delete(this.state.patient).then(this.handleBack),
      title: 'Confirmar remoção?'
    })
  }

  @boundMethod
  public handleEdit (): void {
    this.setState({
      blocked: false
    })
  }

  @boundMethod
  public handleError (): void {
    this.setState({
      blocked: false,
      loading: false
    })
  }

  @boundMethod
  public handleFinish (patient: Partial<Type.Patient>): void {
    this.setState({
      blocked: true,
      loading: true
    }, () => {
      if (this.state.patient) {
        this.difference(patient).then(patient => {
          this.update({
            ...patient,
            identifier: this.state.patient?.identifier
          }).then(this.handleBack).catch(this.handleError)
        })
      } else {
        this.create(patient).then(this.handleBack).catch(this.handleError)
      }
    })
  }

  @boundMethod
  public handleSuccess (response: AxiosResponse<Type.Patient>): void {
    this.setState({
      loading: false,
      patient: response.data
    }, () => {
      if (this.form.current && this.state.patient) {
        this.form.current.setFieldsValue(this.state.patient)
      }
    })
  }

  public render (): ReactNode {
    if (this.state.regress) {
      return (
        <Redirect
          push={true}
          to="/patients"
        />
      )
    }
    return (
      <ConfigConsumer>
        {this.renderContextConsumer}
      </ConfigConsumer>
    )
  }

  @boundMethod
  public renderContextConsumer (configuration: ConfigConsumerProps): ReactNode {
    return (
      <div
        className={configuration.getPrefixCls('patient-unique')}
      >
        <PageHeader
          extra={this.renderExtra()}
          onBack={this.handleBack}
          title="Paciente"
        >
          {this.renderEmpty()}
          {this.renderForm()}
        </PageHeader>
      </div>
    )
  }

  @boundMethod
  public renderEmpty (): ReactNode {
    if (this.props.match.params.patient_identifier && !this.state.loading && !this.state.patient) {
      return (
        <Empty
          image={Empty.PRESENTED_IMAGE_SIMPLE}
        />
      )
    }
    return null
  }

  @boundMethod
  public renderExtra (): ReactNode {
    if (!this.state.loading && this.state.blocked && this.state.patient) {
      return (
        <>
          <Button
            icon={<EditOutlined />}
            onClick={this.handleEdit}
            type="text"
          />
          <Button
            danger={true}
            icon={<DeleteOutlined />}
            onClick={this.handleDelete}
            type="text"
          />
        </>
      )
    }
    return null
  }

  @boundMethod
  public renderFieldBirth (): ReactNode {
    return (
      <Form.Item
        hasFeedback={!this.state.blocked}
        label="Data de nascimento"
        name="birth"
        rules={[{
          required: true
        }]}
      >
        <DatePicker
          disabled={this.state.blocked}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldEthnicity (): ReactNode {
    return (
      <Form.Item
        hasFeedback={!this.state.blocked}
        label="Etnia"
        name="ethnicity"
        rules={[{
          required: true
        }]}
      >
        <Select
          disabled={this.state.blocked}
        >
          <Select.Option
            value="black"
          >
            Preto
          </Select.Option>
          <Select.Option
            value="brown"
          >
            Pardo
          </Select.Option>
          <Select.Option
            value="indigenous"
          >
            Indígena
          </Select.Option>
          <Select.Option
            value="white"
          >
            Branco
          </Select.Option>
          <Select.Option
            value="yellow"
          >
            Amarelo
          </Select.Option>
        </Select>
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldFirstName (): ReactNode {
    return (
      <Form.Item
        hasFeedback={!this.state.blocked}
        label="Primeiro nome"
        name="first_name"
        rules={[{
          required: true
        }]}
      >
        <Input
          disabled={this.state.blocked}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldGender (): ReactNode {
    return (
      <Form.Item
        hasFeedback={!this.state.blocked}
        label="Gênero"
        name="gender"
        rules={[{
          required: true
        }]}
      >
        <Select
          disabled={this.state.blocked}
        >
          <Select.Option
            value="female"
          >
            Feminino
          </Select.Option>
          <Select.Option
            value="male"
          >
            Masculino
          </Select.Option>
          <Select.Option
            value="other"
          >
            Outro
          </Select.Option>
        </Select>
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldLastName (): ReactNode {
    return (
      <Form.Item
        hasFeedback={!this.state.blocked}
        label="Último nome"
        name="last_name"
        rules={[{
          required: true
        }]}
      >
        <Input
          disabled={this.state.blocked}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldMaritalStatus (): ReactNode {
    return (
      <Form.Item
        hasFeedback={!this.state.blocked}
        label="Estado civil"
        name="marital_status"
        rules={[{
          required: true
        }]}
      >
        <Select
          disabled={this.state.blocked}
        >
          <Select.Option
            value="divorced"
          >
            Divorciado
          </Select.Option>
          <Select.Option
            value="married"
          >
            Casado
          </Select.Option>
          <Select.Option
            value="separated"
          >
            Separado
          </Select.Option>
          <Select.Option
            value="single"
          >
            Solteiro
          </Select.Option>
          <Select.Option
            value="widowed"
          >
            Viúvo
          </Select.Option>
        </Select>
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldSubmit (): ReactNode {
    if (!this.state.blocked || (this.state.blocked && this.state.loading)) {
      return (
        <Form.Item>
          <Button
            block={true}
            htmlType="submit"
            loading={this.state.loading}
            type="primary"
          >
            Concluir
          </Button>
        </Form.Item>
      )
    }
    return null
  }

  @boundMethod
  public renderForm (): ReactNode {
    if (!this.props.match.params.patient_identifier || this.state.patient) {
      return (
        <Form
          layout="vertical"
          onFinish={this.handleFinish}
          ref={this.form}
          scrollToFirstError={true}
        >
          {this.renderFieldFirstName()}
          {this.renderFieldLastName()}
          {this.renderFieldBirth()}
          {this.renderFieldEthnicity()}
          {this.renderFieldGender()}
          {this.renderFieldMaritalStatus()}
          {this.renderFieldSubmit()}
        </Form>
      )
    }
    return null
  }

  @boundMethod
  public update (patient: Partial<Type.Patient>): Promise<AxiosResponse> {
    return this.props.connection.patch(`/patients/${patient.identifier}`, {
      ...patient,
      identifier: undefined
    })
  }
}
