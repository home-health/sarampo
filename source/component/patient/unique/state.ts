import { Type } from './'

export interface State {
  blocked: boolean
  loading: boolean
  patient?: Type.Patient
  regress: boolean
}
