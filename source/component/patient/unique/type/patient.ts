import { Patient } from '../../../../model'

export type Patient = Omit<Patient.Definition, 'created_at' | 'updated_at'>
