import { Arguments } from './arguments'
import { AxiosInstance } from 'axios'
import { RouteComponentProps } from 'react-router-dom'

export interface Properties extends RouteComponentProps<Arguments> {
  connection: AxiosInstance
}
