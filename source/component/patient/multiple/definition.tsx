import './appearance.less'
import { Button, List, PageHeader } from 'antd'
import { ConfigConsumer, ConfigConsumerProps } from 'antd/lib/config-provider'
import { EnvironmentOutlined, FileOutlined, PlusOutlined } from '@ant-design/icons'
import { Properties, State, Type } from './'
import React, { Component, ReactNode } from 'react'
import { AxiosResponse } from 'axios'
import { Link } from 'react-router-dom'
import { boundMethod } from 'autobind-decorator'

export class Definition extends Component<Properties, State> {
  constructor (properties: Properties) {
    super(properties)
    this.state = {
      loading: true,
      patients: []
    }
  }

  public componentDidMount (): void {
    this.fetch().then(this.handleSuccess).catch(this.handleError)
  }

  @boundMethod
  public fetch (): Promise<AxiosResponse<Array<Type.Patient>>> {
    return this.props.connection.get('/patients', {
      params: {
        fields: [
          'first_name',
          'identifier',
          'last_name',
          'updated_at'
        ]
      }
    })
  }

  @boundMethod
  public handleError (): void {
    this.setState({
      loading: false
    })
  }

  @boundMethod
  public handleSuccess (response: AxiosResponse<Array<Type.Patient>>): void {
    this.setState({
      loading: false,
      patients: response.data
    })
  }

  public render (): ReactNode {
    return (
      <ConfigConsumer>
        {this.renderContextConsumer}
      </ConfigConsumer>
    )
  }

  @boundMethod
  public renderContextConsumer (configuration: ConfigConsumerProps): ReactNode {
    return (
      <div
        className={configuration.getPrefixCls('patient-multiple')}
      >
        <PageHeader
          extra={this.renderExtra()}
          title="Pacientes"
        >
          {this.renderList()}
        </PageHeader>
      </div>
    )
  }

  public renderExtra (): ReactNode {
    return (
      <Link
        to="/patients/create"
      >
        <Button
          icon={<PlusOutlined />}
          type="text"
        />
      </Link>
    )
  }

  @boundMethod
  public renderItem (patient: Type.Patient): ReactNode {
    return (
      <List.Item
        actions={this.renderItemActions(patient)}
      >
        <List.Item.Meta
          description={this.renderItemDescription(patient)}
          title={this.renderItemTitle(patient)}
        />
      </List.Item>
    )
  }

  public renderItemActions (patient: Type.Patient): Array<ReactNode> {
    return [(
      <Link
        key={0}
        to={`/patients/${patient.identifier}/addresses`}
      >
        <Button
          icon={<EnvironmentOutlined />}
          type="text"
        />
      </Link>
    ), (
      <Link
        key={1}
        to={`/patients/${patient.identifier}/documents`}
      >
        <Button
          icon={<FileOutlined />}
          type="text"
        />
      </Link>
    )]
  }

  public renderItemDescription (patient: Type.Patient): ReactNode {
    return `Modificado há ${patient.updated_at.fromNow(true)}`
  }

  public renderItemTitle (patient: Type.Patient): ReactNode {
    return (
      <Link
        to={`/patients/${patient.identifier}`}
      >
        {`${patient.first_name} ${patient.last_name}`}
      </Link>
    )
  }

  @boundMethod
  public renderList (): ReactNode {
    return (
      <List
        dataSource={this.state.patients}
        loading={this.state.loading}
        renderItem={this.renderItem}
      />
    )
  }
}
