import { Type } from './'

export interface State {
  loading: boolean
  patients: Array<Type.Patient>
}
