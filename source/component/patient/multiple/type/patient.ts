import { Patient } from '../../../../model'

export type Patient = Pick<Patient.Definition, 'first_name' | 'identifier' | 'last_name' | 'updated_at'>
