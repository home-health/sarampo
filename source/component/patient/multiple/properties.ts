import { AxiosInstance } from 'axios'

export interface Properties {
  connection: AxiosInstance
}
