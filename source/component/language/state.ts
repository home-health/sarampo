import { Locale } from 'antd/lib/locale-provider'

export interface State {
  locale: Locale
}
