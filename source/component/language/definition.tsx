import 'moment/locale/pt-br'
import { Properties, State } from './'
import React, { Component, ReactNode } from 'react'
import { ConfigProvider } from 'antd'
import { Connection } from '../'
import { Language } from '../../model'
import { Locale } from 'antd/lib/locale-provider'
import Moment from 'moment'
import { boundMethod } from 'autobind-decorator'

export class Definition extends Component<Properties, State> {
  constructor (properties: Properties) {
    super(properties)
    this.state = {
      locale: Language.Resource.Brazil.Portuguese
    }
  }

  public render (): ReactNode {
    return (
      <Language.Context.Provider
        value={{
          locale: this.state.locale,
          update: this.update
        }}
      >
        <ConfigProvider
          locale={this.state.locale}
        >
          <Connection.Definition />
        </ConfigProvider>
      </Language.Context.Provider>
    )
  }

  @boundMethod
  public update (locale: Locale): void {
    this.setState({
      locale
    }, () => {
      Moment.locale(this.state.locale.locale)
    })
  }
}
