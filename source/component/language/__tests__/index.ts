import * as exported from '../'

describe('Language', () => {
  test('Export', () => {
    expect(Object.keys(exported)).toMatchSnapshot()
  })
})
