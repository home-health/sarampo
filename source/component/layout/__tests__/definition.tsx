import { Definition } from '../definition'
import { MemoryRouter } from 'react-router-dom'
import React from 'react'
import { create } from 'react-test-renderer'

describe('Layout', () => {
  describe('Definition', () => {
    test('Render', () => {
      expect(create(
        <MemoryRouter>
          <Definition />
        </MemoryRouter>
      ).toJSON()).toMatchSnapshot()
    })
  })
})
