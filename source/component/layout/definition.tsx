import './appearance.less'
import { Content, Header } from '../'
import { Properties, State } from './'
import React, { Component, ReactNode } from 'react'
import { Layout } from 'antd'

export class Definition extends Component<Properties, State> {
  public render (): ReactNode {
    return (
      <Layout>
        <Header.Definition />
        <Content.Definition />
      </Layout>
    )
  }
}
