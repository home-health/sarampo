import { Properties, State } from './'
import React, { Component, ReactNode } from 'react'
import { Connection } from '../../model'
import { Redirect } from 'react-router-dom'
import { boundMethod } from 'autobind-decorator'

export class Definition extends Component<Properties, State> {
  public render (): ReactNode {
    return (
      <Connection.Context.Consumer>
        {this.renderContextConsumer}
      </Connection.Context.Consumer>
    )
  }

  @boundMethod
  public renderContextConsumer (accessor: Connection.Accessor): ReactNode {
    if (accessor.authorized()) {
      return (
        <Redirect
          to="/dashboard"
        />
      )
    }
    return (
      <Redirect
        to="/login"
      />
    )
  }
}
