import { Account } from '../../../model'

export type Account = Partial<Pick<Account.Definition, 'email_address' | 'password'>>
