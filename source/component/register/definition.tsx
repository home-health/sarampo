import './appearance.less'
import { AxiosError, AxiosResponse } from 'axios'
import { Button, Form, Input, PageHeader } from 'antd'
import { ConfigConsumer, ConfigConsumerProps } from 'antd/lib/config-provider'
import { Properties, State, Type } from './'
import React, { Component, ReactNode, createRef } from 'react'
import { FormInstance } from 'antd/lib/form'
import { Redirect } from 'react-router-dom'
import { boundMethod } from 'autobind-decorator'

export class Definition extends Component<Properties, State> {
  private readonly form = createRef<FormInstance>()

  constructor (properties: Properties) {
    super(properties)
    this.state = {
      loading: false,
      regress: false
    }
  }

  @boundMethod
  public create (account: Type.Account): Promise<AxiosResponse> {
    return this.props.connection.post('/accounts', account)
  }

  @boundMethod
  public handleBack (): void {
    this.setState({
      regress: true
    })
  }

  @boundMethod
  public handleError (error: AxiosError): void {
    if (error.response?.status === 409 && this.form.current) {
      this.form.current.setFields([{
        errors: [
          'E-mail vinculado à outra conta'
        ],
        name: 'email_address'
      }])
    }
  }

  @boundMethod
  public handleFinish (account: Type.Account): void {
    this.setState({
      loading: true
    }, () => {
      this.create(account).then(this.handleBack).catch(this.handleError).finally(() => {
        this.setState({
          loading: false
        })
      })
    })
  }

  public render (): ReactNode {
    if (this.state.regress) {
      return (
        <Redirect
          push={true}
          to="/"
        />
      )
    }
    return (
      <ConfigConsumer>
        {this.renderContextConsumer}
      </ConfigConsumer>
    )
  }

  @boundMethod
  public renderContextConsumer (configuration: ConfigConsumerProps): ReactNode {
    return (
      <div
        className={configuration.getPrefixCls('register')}
      >
        <PageHeader
          onBack={this.handleBack}
          title="Criar uma conta"
        >
          <Form
            layout="vertical"
            onFinish={this.handleFinish}
            ref={this.form}
            scrollToFirstError={true}
          >
            {this.renderFieldEmailAddress()}
            {this.renderFieldPassword()}
            {this.renderFieldSubmit()}
          </Form>
        </PageHeader>
      </div>
    )
  }

  @boundMethod
  public renderFieldEmailAddress (): ReactNode {
    return (
      <Form.Item
        hasFeedback={true}
        label="E-mail"
        name="email_address"
        rules={[{
          required: true
        }, {
          type: 'email'
        }]}
      >
        <Input
          disabled={this.state.loading}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldPassword (): ReactNode {
    return (
      <Form.Item
        hasFeedback={true}
        label="Senha"
        name="password"
        rules={[{
          required: true
        }, {
          min: 8
        }]}
      >
        <Input.Password
          disabled={this.state.loading}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldSubmit (): ReactNode {
    return (
      <Form.Item>
        <Button
          block={true}
          htmlType="submit"
          loading={this.state.loading}
          type="primary"
        >
          Concluir
        </Button>
      </Form.Item>
    )
  }
}
