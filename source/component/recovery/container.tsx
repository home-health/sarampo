import { Parameters, Properties } from './'
import React, { Component, ComponentClass, LazyExoticComponent, ReactNode, Suspense, lazy } from 'react'
import { Connection } from '../../model'
import { Redirect } from 'react-router-dom'
import { boundMethod } from 'autobind-decorator'

export class Container extends Component<Parameters> {
  public getDefinition (): LazyExoticComponent<ComponentClass<Properties>> {
    return lazy(() => import('./definition').then(module => ({ default: module.Definition })))
  }

  public render (): ReactNode {
    return (
      <Connection.Context.Consumer>
        {this.renderContextConsumer}
      </Connection.Context.Consumer>
    )
  }

  @boundMethod
  public renderContextConsumer (accessor: Connection.Accessor): ReactNode {
    if (accessor.authorized()) {
      return (
        <Redirect
          to="/"
        />
      )
    }
    const Definition = this.getDefinition()
    return (
      <Suspense
        fallback={null}
      >
        <Definition
          connection={accessor.connection}
        />
      </Suspense>
    )
  }
}
