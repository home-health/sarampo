import './appearance.less'
import { AxiosError, AxiosResponse } from 'axios'
import { Button, Form, Input, PageHeader, message } from 'antd'
import { ConfigConsumer, ConfigConsumerProps } from 'antd/lib/config-provider'
import { Properties, State, Type } from './'
import React, { Component, ReactNode, createRef } from 'react'
import { RuleObject, StoreValue } from 'rc-field-form/lib/interface'
import { Credential } from '../../model'
import { FormInstance } from 'antd/lib/form'
import { Redirect } from 'react-router-dom'
import { boundMethod } from 'autobind-decorator'

export class Definition extends Component<Properties, State> {
  private readonly form = createRef<FormInstance>()

  constructor (properties: Properties) {
    super(properties)
    this.state = {
      emailed: [],
      loading: false,
      regress: false
    }
  }

  @boundMethod
  public create (account: Pick<Type.Account, 'email_address'>): Promise<AxiosResponse> {
    return new Promise((resolve: (value?: AxiosResponse) => void, reject: (reason?: Error) => void) => {
      this.props.connection.post('/validations', account).then((value?: AxiosResponse) => {
        this.setState({
          emailed: [
            ...this.state.emailed,
            account.email_address
          ]
        }, () => {
          message.info('Verifique sua caixa de entrada')
          resolve(value)
        })
      }).catch(reject)
    })
  }

  public decode (encoded: string): Promise<Credential.Definition> {
    return new Promise((resolve: (value: Credential.Definition) => void, reject: (reason: Error) => void) => {
      try {
        resolve(JSON.parse(atob(encoded.split('.')[1])))
      } catch (error) {
        reject(error)
      }
    })
  }

  @boundMethod
  public handleBack (): void {
    this.setState({
      regress: true
    })
  }

  @boundMethod
  public handleError (error: AxiosError): void {
    if (error.response?.status === 401 && this.form.current) {
      this.form.current.setFields([{
        errors: [
          'Validade expirada'
        ],
        name: 'credential'
      }])
    }
  }

  @boundMethod
  public handleFinish (received: Partial<Pick<Type.Account, 'password'> & Type.Session>): void {
    this.setState({
      loading: true
    }, () => {
      this.decode(received.credential || '').then((credential: Credential.Definition) => {
        this.update({
          identifier: credential.account_identifier,
          password: received.password
        }, {
          credential: received.credential
        }).then(this.handleBack).catch(this.handleError).finally(() => {
          this.setState({
            loading: false
          })
        })
      })
    })
  }

  public render (): ReactNode {
    if (this.state.regress) {
      return (
        <Redirect
          push={true}
          to="/"
        />
      )
    }
    return (
      <ConfigConsumer>
        {this.renderContextConsumer}
      </ConfigConsumer>
    )
  }

  @boundMethod
  public renderContextConsumer (configuration: ConfigConsumerProps): ReactNode {
    return (
      <div
        className={configuration.getPrefixCls('recovery')}
      >
        <PageHeader
          onBack={this.handleBack}
          title="Recuperar acesso"
        >
          <Form
            layout="vertical"
            onFinish={this.handleFinish}
            ref={this.form}
            scrollToFirstError={true}
          >
            {this.renderFieldEmailAddress()}
            {this.renderFieldCredential()}
            {this.renderFieldPassword()}
            {this.renderFieldSubmit()}
          </Form>
        </PageHeader>
      </div>
    )
  }

  @boundMethod
  public renderFieldCredential (): ReactNode {
    return (
      <Form.Item
        hasFeedback={true}
        label="Credencial de validação"
        name="credential"
        rules={[{
          required: true
        }, {
          message: 'Formato inválido',
          validator: this.validateFieldCredential
        }]}
      >
        <Input
          disabled={this.state.loading}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldEmailAddress (): ReactNode {
    return (
      <Form.Item
        hasFeedback={true}
        label="E-mail"
        name="email_address"
        rules={[{
          required: true
        }, {
          type: 'email'
        }, {
          message: 'E-mail não encontrado',
          validateTrigger: 'onBlur',
          validator: this.validateFieldEmailAddress
        }]}
        validateFirst={true}
        validateTrigger={[
          'onBlur',
          'onChange'
        ]}
      >
        <Input
          disabled={this.state.loading}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldPassword (): ReactNode {
    return (
      <Form.Item
        hasFeedback={true}
        label="Nova senha"
        name="password"
        rules={[{
          required: true
        }, {
          min: 8
        }]}
      >
        <Input.Password
          disabled={this.state.loading}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldSubmit (): ReactNode {
    return (
      <Form.Item>
        <Button
          block={true}
          htmlType="submit"
          loading={this.state.loading}
          type="primary"
        >
          Concluir
        </Button>
      </Form.Item>
    )
  }

  @boundMethod
  public update (account: Partial<Pick<Type.Account, 'identifier' | 'password'>>, session: Partial<Type.Session>): Promise<AxiosResponse> {
    return this.props.connection.patch(`/accounts/${account.identifier}`, {
      ...account,
      identifier: undefined
    }, {
      headers: {
        Authorization: `Bearer ${session.credential}`
      }
    })
  }

  @boundMethod
  public validateFieldCredential (rule: RuleObject, value: StoreValue): Promise<void> {
    return new Promise((resolve: () => void, reject: (reason: Error) => void) => {
      if (value) {
        this.decode(value).then(resolve).catch(reject)
      } else {
        resolve()
      }
    })
  }

  @boundMethod
  public validateFieldEmailAddress (rule: RuleObject, value: StoreValue): Promise<void> {
    return new Promise((resolve: () => void, reject: (reason: Error) => void) => {
      if (!value || !this.form.current || this.form.current.getFieldError('email_address').length || this.state.emailed.includes(value)) {
        resolve()
      } else {
        this.form.current.setFields([{
          name: 'email_address',
          validating: true
        }])
        this.create({
          email_address: value
        }).then(resolve).catch(reject)
      }
    })
  }
}
