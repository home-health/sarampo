export interface State {
  emailed: Array<string | undefined>
  loading: boolean
  regress: boolean
}
