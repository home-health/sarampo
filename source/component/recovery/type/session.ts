import { Session } from '../../../model'

export interface Session extends Pick<Session.Definition, 'credential'> {}
