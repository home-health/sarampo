import { Account } from '../../../model'

export interface Account extends Pick<Account.Definition, 'email_address' | 'identifier' | 'password'> {}
