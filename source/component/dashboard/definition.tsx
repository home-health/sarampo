import { ConfigConsumer, ConfigConsumerProps } from 'antd/lib/config-provider'
import { Properties, State } from './'
import React, { Component, ReactNode } from 'react'

export class Definition extends Component<Properties, State> {
  public render (): ReactNode {
    return (
      <ConfigConsumer>
        {this.renderContextConsumer}
      </ConfigConsumer>
    )
  }

  public renderContextConsumer (configuration: ConfigConsumerProps): ReactNode {
    return (
      <div
        className={configuration.getPrefixCls('dashboard')}
      />
    )
  }
}
