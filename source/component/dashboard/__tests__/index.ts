import * as exported from '../'

describe('Dashboard', () => {
  test('Export', () => {
    expect(Object.keys(exported)).toMatchSnapshot()
  })
})
