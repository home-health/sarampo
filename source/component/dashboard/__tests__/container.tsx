import { Container } from '../container'
import { MemoryRouter } from 'react-router-dom'
import React from 'react'
import { create } from 'react-test-renderer'

describe('Dashboard', () => {
  describe('Container', () => {
    test('Render', () => {
      expect(create(
        <MemoryRouter>
          <Container />
        </MemoryRouter>
      ).toJSON()).toMatchSnapshot()
    })
  })
})
