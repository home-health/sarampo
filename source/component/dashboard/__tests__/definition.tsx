import { Definition } from '../definition'
import React from 'react'
import { create } from 'react-test-renderer'

describe('Dashboard', () => {
  describe('Definition', () => {
    test('Render', () => {
      expect(create(<Definition />).toJSON()).toMatchSnapshot()
    })
  })
})
