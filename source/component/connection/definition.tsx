import { Properties, State } from './'
import React, { Component, ReactNode } from 'react'
import { AxiosInstance } from 'axios'
import { Connection } from '../../model'
import { Layout } from '../'
import { boundMethod } from 'autobind-decorator'

export class Definition extends Component<Properties, State> {
  constructor (properties: Properties) {
    super(properties)
    this.state = {
      connection: new Connection.Factory().create()
    }
  }

  @boundMethod
  public authorized (): boolean {
    return 'Authorization' in this.state.connection.defaults.headers
  }

  public render (): ReactNode {
    return (
      <Connection.Context.Provider
        value={{
          authorized: this.authorized,
          connection: this.state.connection,
          update: this.update
        }}
      >
        <Layout.Definition />
      </Connection.Context.Provider>
    )
  }

  @boundMethod
  public update (connection: AxiosInstance): void {
    this.setState({
      connection
    })
  }
}
