import * as exported from '../'

describe('Connection', () => {
  test('Export', () => {
    expect(Object.keys(exported)).toMatchSnapshot()
  })
})
