import { AxiosInstance } from 'axios'

export interface State {
  connection: AxiosInstance
}
