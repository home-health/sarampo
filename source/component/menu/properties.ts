import { RouteComponentProps } from 'react-router-dom'

export interface Properties extends RouteComponentProps {}
