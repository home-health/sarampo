import { Parameters, Properties, State } from './'
import React, { Component, ComponentClass, LazyExoticComponent, ReactNode, Suspense, lazy } from 'react'
import { RouteComponentProps, WithRouterStatics } from 'react-router'
import { Connection } from '../../model'
import { boundMethod } from 'autobind-decorator'
import { withRouter } from 'react-router-dom'

export class Container extends Component<Parameters> {
  public getDefinition (): ComponentClass<Omit<Properties, keyof RouteComponentProps>, State> & WithRouterStatics<LazyExoticComponent<ComponentClass<Properties, State>>> {
    return withRouter(lazy(() => import('./definition').then(module => ({ default: module.Definition }))))
  }

  public render (): ReactNode {
    return (
      <Connection.Context.Consumer>
        {this.renderContextConsumer}
      </Connection.Context.Consumer>
    )
  }

  @boundMethod
  public renderContextConsumer (accessor: Connection.Accessor): ReactNode {
    if (accessor.authorized()) {
      const Definition = this.getDefinition()
      return (
        <Suspense
          fallback={null}
        >
          <Definition />
        </Suspense>
      )
    }
    return null
  }
}
