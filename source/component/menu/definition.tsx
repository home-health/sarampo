import { Button, Drawer, Menu } from 'antd'
import Icon, { BellOutlined, DashboardOutlined, DesktopOutlined, LockOutlined, MenuOutlined, QuestionCircleOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons'
import { Properties, State } from './'
import React, { Component, ReactNode } from 'react'
import { Link } from 'react-router-dom'
import { Vector } from '../'
import { boundMethod } from 'autobind-decorator'

export class Definition extends Component<Properties, State> {
  constructor (properties: Properties) {
    super(properties)
    this.state = {
      visible: false
    }
  }

  @boundMethod
  public toggle (): void {
    this.setState({
      visible: !this.state.visible
    })
  }

  public render (): ReactNode {
    return (
      <>
        <Button
          icon={<MenuOutlined />}
          onClick={this.toggle}
        />
        <Drawer
          closable={false}
          onClose={this.toggle}
          placement="left"
          title={null}
          visible={this.state.visible}
        >
          <Menu
            mode="inline"
            onSelect={this.toggle}
            selectedKeys={
              new Array().concat(
                this.props.location.pathname,
                this.props.location.pathname.match(/^\/\w+/g)
              )
            }
          >
            <Menu.Item
              icon={<DashboardOutlined />}
              key="/dashboard"
            >
              <Link
                to="/dashboard"
              >
                Painel de controle
              </Link>
            </Menu.Item>
            <Menu.Item
              icon={<Icon component={Vector.Wheelchair.Definition} />}
              key="/patients"
            >
              <Link
                to="/patients"
              >
                Pacientes
              </Link>
            </Menu.Item>
            <Menu.SubMenu
              icon={<SettingOutlined />}
              title="Configurações"
            >
              <Menu.Item
                icon={<UserOutlined />}
                key="/settings/profile"
              >
                <Link
                  to="/settings/profile"
                >
                  Perfil
                </Link>
              </Menu.Item>
              <Menu.Item
                icon={<DesktopOutlined />}
                key="/settings/display"
              >
                <Link
                  to="/settings/display"
                >
                  Interface
                </Link>
              </Menu.Item>
              <Menu.Item
                icon={<LockOutlined />}
                key="/settings/security"
              >
                <Link
                  to="/settings/security"
                >
                  Segurança
                </Link>
              </Menu.Item>
              <Menu.Item
                icon={<BellOutlined />}
                key="/settings/notifications"
              >
                <Link
                  to="/settings/notifications"
                >
                  Notificações
                </Link>
              </Menu.Item>
            </Menu.SubMenu>
            <Menu.Item
              icon={<QuestionCircleOutlined />}
              key="/help"
            >
              <Link
                to="/help"
              >
                Ajuda
              </Link>
            </Menu.Item>
          </Menu>
        </Drawer>
      </>
    )
  }
}
