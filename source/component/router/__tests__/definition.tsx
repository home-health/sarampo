import { Definition } from '../definition'
import React from 'react'
import { create } from 'react-test-renderer'

describe('Router', () => {
  describe('Definition', () => {
    describe('Render', () => {
      describe('History', () => {
        test('Memory', () => {
          expect(create(
            <Definition
              history="memory"
            />
          ).toJSON()).toMatchSnapshot()
        })
      })
    })
  })
})
