import * as exported from '../'

describe('Router', () => {
  test('Export', () => {
    expect(Object.keys(exported)).toMatchSnapshot()
  })
})
