import { History, createBrowserHistory, createHashHistory, createMemoryHistory } from 'history'
import { Properties, State } from './'
import React, { Component, ReactNode } from 'react'
import { Language } from '../'
import { Router } from 'react-router-dom'

export class Definition extends Component<Properties, State> {
  public getHistory (type: 'browser' | 'hash' | 'memory'): History {
    switch (type) {
      case 'browser':
        return createBrowserHistory()
      case 'hash':
        return createHashHistory()
      case 'memory':
        return createMemoryHistory()
    }
  }

  public render (): ReactNode {
    return (
      <Router
        history={this.getHistory(this.props.history)}
      >
        <Language.Definition />
      </Router>
    )
  }
}
