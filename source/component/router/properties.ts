export interface Properties {
  history: 'browser' | 'hash' | 'memory'
}
