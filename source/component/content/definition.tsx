import './appearance.less'
import { Dashboard, Login, Patient, Recovery, Redirect, Register } from '../'
import { Properties, State } from './'
import React, { Component, ReactNode } from 'react'
import { Route, Switch } from 'react-router-dom'
import { Layout } from 'antd'

export class Definition extends Component<Properties, State> {
  public render (): ReactNode {
    return (
      <Layout.Content>
        <Switch>
          <Route
            exact={true}
            path="/dashboard"
          >
            <Dashboard.Container />
          </Route>
          <Route
            exact={true}
            path="/login"
          >
            <Login.Container />
          </Route>
          <Route
            exact={true}
            path="/patients"
          >
            <Patient.Multiple.Container />
          </Route>
          <Route
            exact={true}
            path="/patients/create"
          >
            <Patient.Unique.Container />
          </Route>
          <Route
            exact={true}
            path="/patients/:patient_identifier"
          >
            <Patient.Unique.Container />
          </Route>
          <Route
            exact={true}
            path="/recovery"
          >
            <Recovery.Container />
          </Route>
          <Route
            exact={true}
            path="/register"
          >
            <Register.Container />
          </Route>
          <Route
            exact={true}
            path="/"
          >
            <Redirect.Definition />
          </Route>
        </Switch>
      </Layout.Content>
    )
  }
}
