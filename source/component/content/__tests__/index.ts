import * as exported from '../'

describe('Content', () => {
  test('Export', () => {
    expect(Object.keys(exported)).toMatchSnapshot()
  })
})
