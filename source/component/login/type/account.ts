import { Account } from '../../../model'

export interface Account extends Partial<Pick<Account.Definition, 'email_address' | 'password'>> {}
