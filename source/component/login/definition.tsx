import './appearance.less'
import { AxiosError, AxiosResponse } from 'axios'
import { Button, Form, Input, PageHeader } from 'antd'
import { ConfigConsumer, ConfigConsumerProps } from 'antd/lib/config-provider'
import { Connection, Session } from '../../model'
import { Properties, State, Type } from './'
import React, { Component, ReactNode, createRef } from 'react'
import { FormInstance } from 'antd/lib/form'
import { Link } from 'react-router-dom'
import { boundMethod } from 'autobind-decorator'

export class Definition extends Component<Properties, State> {
  private readonly form = createRef<FormInstance>()

  constructor (properties: Properties) {
    super(properties)
    this.state = {
      loading: false
    }
  }

  @boundMethod
  public create (account: Type.Account): Promise<AxiosResponse<Session.Definition>> {
    return this.props.connection.post('/sessions', account)
  }

  @boundMethod
  public handleError (error: AxiosError): void {
    if (error.response?.status === 403 && this.form.current) {
      this.form.current.setFields([{
        errors: [
          'Este campo pode estar incorreto'
        ],
        name: 'email_address'
      }, {
        errors: [
          'Este campo pode estar incorreto'
        ],
        name: 'password'
      }])
    }
  }

  @boundMethod
  public handleFinish (account: Type.Account): void {
    this.setState({
      loading: true
    }, () => {
      this.create(account).then(this.handleSuccess).catch(this.handleError).finally(() => {
        this.setState({
          loading: false
        })
      })
    })
  }

  @boundMethod
  public handleSuccess (response: AxiosResponse<Session.Definition>): void {
    this.props.onSuccess(
      new Connection.Factory({
        headers: {
          Authorization: `Bearer ${response.data.credential}`
        }
      }).create()
    )
  }

  public render (): ReactNode {
    return (
      <ConfigConsumer>
        {this.renderContextConsumer}
      </ConfigConsumer>
    )
  }

  @boundMethod
  public renderContextConsumer (configuration: ConfigConsumerProps): ReactNode {
    return (
      <div
        className={configuration.getPrefixCls('login')}
      >
        <PageHeader
          footer={this.renderFooter()}
          title="Fazer login"
        >
          <Form
            layout="vertical"
            onFinish={this.handleFinish}
            ref={this.form}
            scrollToFirstError={true}
          >
            {this.renderFieldEmailAddress()}
            {this.renderFieldPassword()}
            {this.renderFieldSubmit()}
          </Form>
        </PageHeader>
      </div>
    )
  }

  @boundMethod
  public renderFieldEmailAddress (): ReactNode {
    return (
      <Form.Item
        dependencies={[
          'password'
        ]}
        hasFeedback={true}
        label="E-mail"
        name="email_address"
        rules={[{
          required: true
        }, {
          type: 'email'
        }]}
      >
        <Input
          disabled={this.state.loading}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldPassword (): ReactNode {
    return (
      <Form.Item
        dependencies={[
          'email_address'
        ]}
        hasFeedback={true}
        label="Senha"
        name="password"
        rules={[{
          required: true
        }, {
          min: 8
        }]}
      >
        <Input.Password
          disabled={this.state.loading}
        />
      </Form.Item>
    )
  }

  @boundMethod
  public renderFieldSubmit (): ReactNode {
    return (
      <Form.Item>
        <Button
          block={true}
          htmlType="submit"
          loading={this.state.loading}
          type="primary"
        >
          Entrar
        </Button>
      </Form.Item>
    )
  }

  public renderFooter (): ReactNode {
    return (
      <>
        <Link
          to="/recovery"
        >
          Esqueci a senha
        </Link>
        <Link
          to="/register"
        >
          Criar uma conta
        </Link>
      </>
    )
  }
}
