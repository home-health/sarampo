const HTML = require('html-webpack-plugin')
const Mini = require('optimize-css-assets-webpack-plugin')
const Pack = require('webpack')
const Path = require('path')
const Pull = require('mini-css-extract-plugin')
const Ugly = require('terser-webpack-plugin')

module.exports = {
  devServer: {
    compress: true,
    historyApiFallback: true,
    host: '0.0.0.0'
  },
  devtool: 'source-map',
  entry: [
    Path.resolve(__dirname, 'source')
  ],
  mode: 'production',
  module: {
    rules: [{
      test: /\.less$/,
      use: [{
        loader: Pull.loader
      }, {
        loader: 'css-loader'
      }, {
        loader: 'less-loader',
        options: {
          lessOptions: {
            javascriptEnabled: true,
            modifyVars: {
              'drawer-body-padding': '0',
              'error-color': process.env.THEME_COLOR_ERROR,
              'highlight-color': process.env.THEME_COLOR_HIGHLIGHT,
              'info-color': process.env.THEME_COLOR_INFORMATION,
              'layout-body-background': '@white',
              'layout-header-background': '@white',
              'layout-header-height': '@height-base + (2 * @padding-lg)',
              'layout-header-padding': '0 @padding-lg',
              'primary-color': process.env.THEME_COLOR_PRIMARY,
              'processing-color': process.env.THEME_COLOR_PROCESSING,
              'success-color': process.env.THEME_COLOR_SUCCESS,
              'warning-color': process.env.THEME_COLOR_WARNING
            }
          }
        }
      }]
    }, {
      exclude: /node_modules/,
      test: /\.tsx?$/,
      use: [{
        loader: 'babel-loader',
        options: {
          plugins: [
            [
              '@babel/plugin-proposal-decorators',
              {
                legacy: true
              }
            ],
            [
              '@babel/plugin-proposal-class-properties',
              {
                loose: true
              }
            ],
            [
              '@babel/plugin-proposal-export-namespace-from'
            ],
            [
              'import',
              {
                libraryName: 'antd',
                style: true
              }
            ]
          ],
          presets: [
            '@babel/preset-env',
            '@babel/preset-react',
            '@babel/preset-typescript'
          ]
        }
      }]
    }]
  },
  optimization: {
    minimizer: [
      new Mini({
        cssProcessorOptions: {
          map: {
            annotation: true,
            inline: false
          }
        }
      }),
      new Ugly({
        extractComments: false,
        terserOptions: {
          output: {
            comments: false
          }
        }
      })
    ],
    runtimeChunk: true,
    splitChunks: {
      chunks: 'all'
    }
  },
  output: {
    chunkFilename: '[contenthash].js',
    filename: '[contenthash].js',
    path: Path.resolve(__dirname, 'build'),
    publicPath: process.env.ROUTER_PATH
  },
  plugins: [
    new HTML({
      inject: 'head',
      title: process.env.APP_NAME
    }),
    new Pack.DefinePlugin({
      process: {
        env: JSON.stringify([
          'API_PATH',
          'APP_NAME',
          'ROUTER_PATH',
          'ROUTER_HISTORY',
          'THEME_COLOR_ERROR',
          'THEME_COLOR_HIGHLIGHT',
          'THEME_COLOR_INFORMATION',
          'THEME_COLOR_PRIMARY',
          'THEME_COLOR_PROCESSING',
          'THEME_COLOR_SUCCESS',
          'THEME_COLOR_WARNING'
        ].reduce((previous, index) => ({
          ...previous,
          [index]: process.env[index]
        }), {}))
      }
    }),
    new Pull({
      chunkFilename: '[contenthash].css',
      filename: '[contenthash].css'
    })
  ],
  resolve: {
    extensions: [
      '.js',
      '.ts',
      '.tsx'
    ]
  }
}
